#define Py_LIMITED_API
#include <Python.h>

static PyObject* showspeed_avg(PyObject *self, PyObject *args);

static PyMethodDef ShowSpeedMethods[] = {
    {"avg", showspeed_avg, METH_VARARGS, "Compute avg"},
    {NULL, NULL, 0, NULL} /* Sentinel */
};

static PyModuleDef showspeed_module = {
    PyModuleDef_HEAD_INIT,
    "showspeed",
    "Example for speedup",
    -1,
    ShowSpeedMethods
};

PyMODINIT_FUNC PyInit_showspeed()
{
    return PyModule_Create(&showspeed_module);
}

static PyObject* showspeed_avg(PyObject *self, PyObject *args)
{
    PyObject* list;
    if (!PyArg_ParseTuple(args, "O", &list)) {
        return NULL;
    }

    if (!PyList_Check(list)) {
        PyErr_SetString(PyExc_ValueError, "arg is not a list");
        return NULL;
    }

    double sum = 0.0;
    auto size = PyList_Size(list);
    if (size == 0) {
        return PyFloat_FromDouble(0.0);
    }

    for (int64_t i = 0; i < size; i++) {
        auto item = PyList_GetItem(list, i);
        if (!PyFloat_Check(item)) {
            PyErr_SetString(PyExc_ValueError, "list element is not float");
            return NULL;
        }

        auto value = PyFloat_AsDouble(item);

        sum += value;
    }

    return PyFloat_FromDouble(sum / size);
}
